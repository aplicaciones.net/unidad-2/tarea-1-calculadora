﻿Public Class Libreria


    'Declarar propiedades
    'Atributo
    Private uno As Int16
    'Propiedad
    Public Property Puno As Int16
        Get
            Return uno
        End Get
        Set(value As Int16)
            uno = value
        End Set
    End Property
    Private dos As Int16
    Public Property Pdos As Int16
        Get
            Return dos
        End Get
        Set(value As Int16)
            dos = value
        End Set
    End Property

    Private tres As Int16
    Public Property Ptres As Int16
        Get
            Return tres
        End Get
        Set(value As Int16)
            tres = value
        End Set
    End Property

    Private vector(3) As Int16
    Public Property Pvector As Int16
        Get
            Return vector(2)
        End Get
        Set(value As Int16)
            tres = value
        End Set
    End Property


    Public Function Calcular(ByRef datos() As Int16,
                             ByVal n As Int16,
                             ByVal operacion As String) As Int16
        Dim resultado As Int16 = 0
        Dim ciclo
        If (operacion.Equals("suma")) Then
            For ciclo = 0 To n Step 1
                resultado += datos(ciclo)
            Next
        End If

        If (operacion.Equals("resta")) Then
            For ciclo = 0 To n Step 1
                resultado -= datos(ciclo)
            Next
        End If

        If (operacion.Equals("multi")) Then
            resultado = (datos(0) * datos(1) * datos(2))
        End If

        If (operacion.Equals("div")) Then
            resultado = datos(0) / datos(1) / datos(2)
        End If

        Return (resultado)

    End Function

    Sub Imprime(ByVal rsuma As Int16)
        Console.Write("La suma es:")
        Console.WriteLine(rsuma)
        Return
    End Sub
End Class
