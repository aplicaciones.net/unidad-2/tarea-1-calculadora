﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class Ventana_Secundaria
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.button_Show = New System.Windows.Forms.Button()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.txt_resultado = New System.Windows.Forms.TextBox()
        Me.button_Regresar = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'button_Show
        '
        Me.button_Show.Location = New System.Drawing.Point(51, 59)
        Me.button_Show.Name = "button_Show"
        Me.button_Show.Size = New System.Drawing.Size(75, 23)
        Me.button_Show.TabIndex = 9
        Me.button_Show.Text = "Mostrar"
        Me.button_Show.UseVisualStyleBackColor = True
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(48, 21)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(61, 13)
        Me.Label1.TabIndex = 8
        Me.Label1.Text = "Respuesta:"
        '
        'txt_resultado
        '
        Me.txt_resultado.Location = New System.Drawing.Point(115, 18)
        Me.txt_resultado.Name = "txt_resultado"
        Me.txt_resultado.Size = New System.Drawing.Size(100, 20)
        Me.txt_resultado.TabIndex = 7
        '
        'button_Regresar
        '
        Me.button_Regresar.Location = New System.Drawing.Point(140, 59)
        Me.button_Regresar.Name = "button_Regresar"
        Me.button_Regresar.Size = New System.Drawing.Size(75, 23)
        Me.button_Regresar.TabIndex = 10
        Me.button_Regresar.Text = "Regresar"
        Me.button_Regresar.UseVisualStyleBackColor = True
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(275, 116)
        Me.Controls.Add(Me.button_Regresar)
        Me.Controls.Add(Me.button_Show)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.txt_resultado)
        Me.Name = "Form1"
        Me.Text = "Form1"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents button_Show As Button
    Friend WithEvents Label1 As Label
    Friend WithEvents txt_resultado As TextBox
    Friend WithEvents button_Regresar As Button
End Class
