﻿Module Main_Clase1

    'Como hacer un constructor nulo en VB
    Class Module1
        Sub New()
        End Sub
    End Class


    Dim a, b, c As Int16

    Function Calculo(ByVal uno As Int16,
                     ByVal dos As Int16,
                     ByVal tres As Int16) As Int16

        Dim suma As Int16
        suma = uno + dos + tres
        'Si devuelve resultado
        Return (suma)
    End Function

    Sub Imprime(ByVal rsuma As Int16)
        Console.WriteLine("La suma es:")
        Console.WriteLine(rsuma)
        'Un sub no puede devolver valores, pero por estetica se puede poner el return
        Return
    End Sub

    Sub Main()

        Dim modulo As New Module1()

        Dim resp As Int16
        Dim vector(5) As Int16
        Console.WriteLine("Dame el primer valor")
        a = Convert.ToInt16(Console.ReadLine())

        Console.WriteLine("Dame el segundo valor")
        b = Convert.ToInt16(Console.ReadLine())

        Console.WriteLine("Dame el tercer valor")
        c = Convert.ToInt16(Console.ReadLine())

        resp = Calculo(a, b, c)
        Imprime(resp)
        Console.ReadLine()

        Return
    End Sub



End Module
