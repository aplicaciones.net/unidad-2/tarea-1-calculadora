﻿Imports Microsoft.VisualBasic.FileIO
Imports System.IO
Imports System.Text

Public Class Ventana_Principal

    Dim uno, dos, tres As Int16
    Dim vector(3) As Int16
    Dim resp As Int16
    Dim operacion As String
    Dim veces As Int16
    Dim current = 1

    Dim valores_1(1) As Int16
    Dim valores_2(1) As Int16
    Dim valores_3(1) As Int16

    Dim obj As New ClassLibrary1.Libreria

    Private Sub dato1_TextChanged(sender As Object, e As EventArgs) Handles dato1.TextChanged
        If (dato1.Text <> "") Then
            ' uno = CType(dato1.Text, Int16)
            ' vector(0) = CType(dato1.Text, Int16)
            obj.Puno = CType(dato1.Text, Int16)
            vector(0) = obj.Puno
        End If
    End Sub

    Private Sub dato2_TextChanged(sender As Object, e As EventArgs) Handles dato2.TextChanged
        If (dato2.Text <> "") Then
            'dos = CType(dato2.Text, Int16)
            ' vector(1) = CType(dato2.Text, Int16)
            obj.Pdos = CType(dato2.Text, Int16)
            vector(1) = obj.Pdos
        End If
    End Sub

    Private Sub dato3_TextChanged(sender As Object, e As EventArgs) Handles dato3.TextChanged
        If (dato3.Text <> "") Then
            'tres = CType(dato3.Text, Int16)
            ' vector(2) = CType(dato3.Text, Int16)
            obj.Ptres = CType(dato3.Text, Int16)
            vector(2) = obj.Ptres
        End If
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        ' Pasar el valor de la suma a la segunda ventana
        Dim Ventana_Sec As New Respuesta_Clase2.Ventana_Secundaria

        resp = obj.Calcular(vector, 3, operacion)
        Ventana_Sec.Recibe(resp)
        ' Ocultar la ventana actual 
        Me.Hide()
        ' Abrir la neva ventana
        Ventana_Sec.Show()

    End Sub

    Private Sub Button_Save_Click(sender As Object, e As EventArgs) Handles Button_Save.Click
        Dim reg As New StreamWriter(Environment.GetFolderPath(Environment.SpecialFolder.Desktop) + "\output.txt", False)
        Dim ciclo As Int16
        For ciclo = 1 To veces Step 1
            reg.WriteLine(valores_1(ciclo - 1))
            reg.WriteLine(valores_2(ciclo - 1))
            reg.WriteLine(valores_3(ciclo - 1))
        Next
        reg.Close()

    End Sub

    Private Sub Button_Read_Click(sender As Object, e As EventArgs) Handles Button_Read.Click
        Dim regdos As New StreamReader(Environment.GetFolderPath(Environment.SpecialFolder.Desktop) + "\output.txt")
        Do Until (regdos.EndOfStream)
            dato1.Text = regdos.ReadLine()
            dato2.Text = regdos.ReadLine()
            dato3.Text = regdos.ReadLine()
            dato1.Refresh()
            dato2.Refresh()
            dato3.Refresh()
            'MessageBox.Show("Pausa")
            Threading.Thread.Sleep(1000)
        Loop
        regdos.Close()
    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles button_CreateBin.Click
        Dim bin As Stream = File.Open(Environment.GetFolderPath(Environment.SpecialFolder.Desktop) + "\output.bin",
                                      FileMode.Create, 'FileMode.Append
                                      FileAccess.Write)
        Dim nomint As New BinaryWriter(bin)

        Dim ciclo As Int16
        For ciclo = 1 To veces Step 1
            nomint.Write(valores_1(ciclo - 1))
            nomint.Write(valores_2(ciclo - 1))
            nomint.Write(valores_3(ciclo - 1))
        Next
        nomint.Close()
    End Sub

    Private Sub Button_ShowBin_Click(sender As Object, e As EventArgs) Handles Button_ShowBin.Click
        Dim bin_Read As Stream = File.Open(Environment.GetFolderPath(Environment.SpecialFolder.Desktop) + "\output.bin",
                                      FileMode.Open,
                                      FileAccess.Read)
        Dim nomint2 As New BinaryReader(bin_Read)
        Do Until (nomint2.PeekChar = -1)
            dato1.Text = nomint2.ReadInt16
            dato2.Text = nomint2.ReadInt16
            dato3.Text = nomint2.ReadInt16
            dato1.Refresh()
            dato2.Refresh()
            dato3.Refresh()
            'MessageBox.Show("Pausa")
            Threading.Thread.Sleep(1000)
        Loop
        nomint2.Close()
    End Sub

    Sub Concatenar()
        valores_1(current - 1) = vector(0)
        valores_2(current - 1) = vector(1)
        valores_3(current - 1) = vector(2)
    End Sub

    Private Sub Button_Suma_Click(sender As Object, e As EventArgs) Handles Button_Suma.Click
        operacion = "suma"
        Concatenar()
        Label_Res.Text = CType(Calcular(vector(0), vector(1), vector(2), operacion), String)

        TurnOff()
        Button1.Enabled = True
    End Sub

    Private Sub Button_Resta_Click(sender As Object, e As EventArgs) Handles Button_Resta.Click
        operacion = "resta"
        Concatenar()
        Label_Res.Text = CType(Calcular(vector(0), vector(1), vector(2), operacion), String)

        TurnOff()
        Button1.Enabled = True
    End Sub

    Private Sub Button_Multiplicar_Click(sender As Object, e As EventArgs) Handles Button_Multiplicar.Click
        operacion = "multi"
        Concatenar()
        Label_Res.Text = CType(Calcular(vector(0), vector(1), vector(2), operacion), String)

        TurnOff()
        Button1.Enabled = True
    End Sub

    Private Sub Button_Dividir_Click(sender As Object, e As EventArgs) Handles Button_Dividir.Click
        operacion = "div"
        Concatenar()
        Label_Res.Text = CType(Calcular(vector(0), vector(1), vector(2), operacion), String)

        TurnOff()
        Button1.Enabled = True
    End Sub

    Private Sub ButtonSet_Click(sender As Object, e As EventArgs) Handles ButtonSet.Click

        If (TextBoxVeces.Text = "") Then
            veces = 1
        ElseIf CType(TextBoxVeces.Text, Int16) <> 0 Then
            veces = CType(TextBoxVeces.Text, Int16)
        End If
        TextBoxVeces.Text = veces
        TextBoxVeces.Enabled = False
        ReDim valores_1(veces)
        ReDim valores_2(veces)
        ReDim valores_3(veces)
    End Sub


    Sub TurnOff()
        If current >= veces Then
            dato1.Enabled = False
            dato2.Enabled = False
            dato3.Enabled = False
        Else
            current += 1
        End If
        LabelVeces.Text = current
    End Sub

    Public Function Calcular(ByVal a As Int16,
                              ByVal b As Int16,
                              ByVal c As Int16,
                             ByVal operacion As String) As Int16
        'Ignores received paramters and takes global vector hehe
        Return (obj.Calcular(vector, 3, operacion))
    End Function

End Class
