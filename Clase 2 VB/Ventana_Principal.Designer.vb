﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Ventana_Principal
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.dato1 = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.dato2 = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.dato3 = New System.Windows.Forms.TextBox()
        Me.Button_Suma = New System.Windows.Forms.Button()
        Me.Label_Res = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.Button_Save = New System.Windows.Forms.Button()
        Me.Button_Read = New System.Windows.Forms.Button()
        Me.button_CreateBin = New System.Windows.Forms.Button()
        Me.Button_ShowBin = New System.Windows.Forms.Button()
        Me.Button_Resta = New System.Windows.Forms.Button()
        Me.Button_Multiplicar = New System.Windows.Forms.Button()
        Me.Button_Dividir = New System.Windows.Forms.Button()
        Me.TextBoxVeces = New System.Windows.Forms.TextBox()
        Me.LabelVeces = New System.Windows.Forms.Label()
        Me.ButtonSet = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'dato1
        '
        Me.dato1.Location = New System.Drawing.Point(81, 53)
        Me.dato1.Name = "dato1"
        Me.dato1.Size = New System.Drawing.Size(100, 20)
        Me.dato1.TabIndex = 0
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(29, 56)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(39, 13)
        Me.Label1.TabIndex = 1
        Me.Label1.Text = "Dato 1"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(29, 94)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(39, 13)
        Me.Label2.TabIndex = 3
        Me.Label2.Text = "Dato 2"
        '
        'dato2
        '
        Me.dato2.Location = New System.Drawing.Point(81, 91)
        Me.dato2.Name = "dato2"
        Me.dato2.Size = New System.Drawing.Size(100, 20)
        Me.dato2.TabIndex = 2
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(29, 134)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(39, 13)
        Me.Label3.TabIndex = 5
        Me.Label3.Text = "Dato 3"
        '
        'dato3
        '
        Me.dato3.Location = New System.Drawing.Point(81, 131)
        Me.dato3.Name = "dato3"
        Me.dato3.Size = New System.Drawing.Size(100, 20)
        Me.dato3.TabIndex = 4
        '
        'Button_Suma
        '
        Me.Button_Suma.Location = New System.Drawing.Point(214, 54)
        Me.Button_Suma.Name = "Button_Suma"
        Me.Button_Suma.Size = New System.Drawing.Size(75, 23)
        Me.Button_Suma.TabIndex = 6
        Me.Button_Suma.Text = "Sumar"
        Me.Button_Suma.UseVisualStyleBackColor = True
        '
        'Label_Res
        '
        Me.Label_Res.AutoSize = True
        Me.Label_Res.Location = New System.Drawing.Point(246, 107)
        Me.Label_Res.Name = "Label_Res"
        Me.Label_Res.Size = New System.Drawing.Size(10, 13)
        Me.Label_Res.TabIndex = 8
        Me.Label_Res.Text = "-"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(214, 80)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(64, 13)
        Me.Label5.TabIndex = 7
        Me.Label5.Text = "La suma es:"
        '
        'Button1
        '
        Me.Button1.Enabled = False
        Me.Button1.Location = New System.Drawing.Point(295, 84)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(237, 36)
        Me.Button1.TabIndex = 9
        Me.Button1.Text = "Ir a Desplegar"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'Button_Save
        '
        Me.Button_Save.Location = New System.Drawing.Point(214, 134)
        Me.Button_Save.Name = "Button_Save"
        Me.Button_Save.Size = New System.Drawing.Size(156, 27)
        Me.Button_Save.TabIndex = 10
        Me.Button_Save.Text = "Guardar"
        Me.Button_Save.UseVisualStyleBackColor = True
        '
        'Button_Read
        '
        Me.Button_Read.Location = New System.Drawing.Point(239, 163)
        Me.Button_Read.Name = "Button_Read"
        Me.Button_Read.Size = New System.Drawing.Size(131, 27)
        Me.Button_Read.TabIndex = 11
        Me.Button_Read.Text = "Mostrar"
        Me.Button_Read.UseVisualStyleBackColor = True
        '
        'button_CreateBin
        '
        Me.button_CreateBin.Location = New System.Drawing.Point(376, 134)
        Me.button_CreateBin.Name = "button_CreateBin"
        Me.button_CreateBin.Size = New System.Drawing.Size(156, 27)
        Me.button_CreateBin.TabIndex = 12
        Me.button_CreateBin.Text = "Guardar Bin"
        Me.button_CreateBin.UseVisualStyleBackColor = True
        '
        'Button_ShowBin
        '
        Me.Button_ShowBin.Location = New System.Drawing.Point(376, 163)
        Me.Button_ShowBin.Name = "Button_ShowBin"
        Me.Button_ShowBin.Size = New System.Drawing.Size(144, 27)
        Me.Button_ShowBin.TabIndex = 13
        Me.Button_ShowBin.Text = "Mostrar Bin"
        Me.Button_ShowBin.UseVisualStyleBackColor = True
        '
        'Button_Resta
        '
        Me.Button_Resta.Location = New System.Drawing.Point(295, 54)
        Me.Button_Resta.Name = "Button_Resta"
        Me.Button_Resta.Size = New System.Drawing.Size(75, 23)
        Me.Button_Resta.TabIndex = 14
        Me.Button_Resta.Text = "Restar"
        Me.Button_Resta.UseVisualStyleBackColor = True
        '
        'Button_Multiplicar
        '
        Me.Button_Multiplicar.Location = New System.Drawing.Point(376, 54)
        Me.Button_Multiplicar.Name = "Button_Multiplicar"
        Me.Button_Multiplicar.Size = New System.Drawing.Size(75, 23)
        Me.Button_Multiplicar.TabIndex = 15
        Me.Button_Multiplicar.Text = "Multiplicar"
        Me.Button_Multiplicar.UseVisualStyleBackColor = True
        '
        'Button_Dividir
        '
        Me.Button_Dividir.Location = New System.Drawing.Point(457, 54)
        Me.Button_Dividir.Name = "Button_Dividir"
        Me.Button_Dividir.Size = New System.Drawing.Size(75, 23)
        Me.Button_Dividir.TabIndex = 16
        Me.Button_Dividir.Text = "Dividir"
        Me.Button_Dividir.UseVisualStyleBackColor = True
        '
        'TextBoxVeces
        '
        Me.TextBoxVeces.Location = New System.Drawing.Point(270, 12)
        Me.TextBoxVeces.Name = "TextBoxVeces"
        Me.TextBoxVeces.Size = New System.Drawing.Size(31, 20)
        Me.TextBoxVeces.TabIndex = 17
        '
        'LabelVeces
        '
        Me.LabelVeces.AutoSize = True
        Me.LabelVeces.Location = New System.Drawing.Point(246, 16)
        Me.LabelVeces.Name = "LabelVeces"
        Me.LabelVeces.Size = New System.Drawing.Size(10, 13)
        Me.LabelVeces.TabIndex = 18
        Me.LabelVeces.Text = "-"
        '
        'ButtonSet
        '
        Me.ButtonSet.Location = New System.Drawing.Point(307, 9)
        Me.ButtonSet.Name = "ButtonSet"
        Me.ButtonSet.Size = New System.Drawing.Size(75, 23)
        Me.ButtonSet.TabIndex = 19
        Me.ButtonSet.Text = "Set"
        Me.ButtonSet.UseVisualStyleBackColor = True
        '
        'Ventana_Principal
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(556, 212)
        Me.Controls.Add(Me.ButtonSet)
        Me.Controls.Add(Me.LabelVeces)
        Me.Controls.Add(Me.TextBoxVeces)
        Me.Controls.Add(Me.Button_Dividir)
        Me.Controls.Add(Me.Button_Multiplicar)
        Me.Controls.Add(Me.Button_Resta)
        Me.Controls.Add(Me.Button_ShowBin)
        Me.Controls.Add(Me.button_CreateBin)
        Me.Controls.Add(Me.Button_Read)
        Me.Controls.Add(Me.Button_Save)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.Label_Res)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.Button_Suma)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.dato3)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.dato2)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.dato1)
        Me.Name = "Ventana_Principal"
        Me.Text = "Form1"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents dato1 As TextBox
    Friend WithEvents Label1 As Label
    Friend WithEvents Label2 As Label
    Friend WithEvents dato2 As TextBox
    Friend WithEvents Label3 As Label
    Friend WithEvents dato3 As TextBox
    Friend WithEvents Button_Suma As Button
    Friend WithEvents Label_Res As Label
    Friend WithEvents Label5 As Label
    Friend WithEvents Button1 As Button
    Friend WithEvents Button_Save As Button
    Friend WithEvents Button_Read As Button
    Friend WithEvents button_CreateBin As Button
    Friend WithEvents Button_ShowBin As Button
    Friend WithEvents Button_Resta As Button
    Friend WithEvents Button_Multiplicar As Button
    Friend WithEvents Button_Dividir As Button
    Friend WithEvents TextBoxVeces As TextBox
    Friend WithEvents LabelVeces As Label
    Friend WithEvents ButtonSet As Button
End Class
