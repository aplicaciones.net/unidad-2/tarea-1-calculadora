﻿Public Class Libreria_2

    Inherits ClassLibrary1.Libreria

    Private datocuatro As Decimal
    Public Property pdatocuatro As Decimal
        Get
            Return datocuatro
        End Get
        Set(value As Decimal)
            datocuatro = value
        End Set
    End Property

    Private datocinco As Decimal
    Public Property pdatocinco As Decimal
        Get
            Return datocinco
        End Get
        Set(value As Decimal)
            datocinco = value
        End Set
    End Property

    Public Function Calcular(ByVal z As Int16, ByVal y As Int16) As Int16
        Dim resp As Decimal
        resp = z * y + (Puno + Pdos + Ptres)
        Return resp
    End Function
End Class
